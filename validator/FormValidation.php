<?php

namespace Star\Validator;

use \Star\Framework\Lang;

//TODO: Add negative validation
//TODO: Convert value type using reference
class FormValidation
{
	protected $data;
	protected $rules = array();
	protected $errors = array();

	public function __construct(array $data_to_validate = null)
	{
		include_once(__DIR__ . '/form_helper.php');
		
		if ($data_to_validate === null)
		{
			$this->data = &$_POST;
		}
		else
		{
			$this->data = &$data_to_validate;
		}
		Lang::load('form_validation');
	}

	//TODO: Validate there are not two set of rules for one field? (Add to existing ruleset)
	public function set_rules($field, $label, $rules)
	{
		// No reason to set rules if we have no data
		if (count($this->data) == 0)
		{
			return $this;
		}
		if (!is_array($rules))
		{
			$rules = explode('|', $rules);
		}

		$this->rules[] = array('field' => $field, 'label' => $label, 'rules' => $rules);
	}

	public function set_file_rules($field, $label, $rules)
	{

	}

	public function run()
	{
		if ($this->data === $_POST && !($_SERVER['REQUEST_METHOD'] === 'POST'))
		{
			return false;
		}
		// No reason to validate if we have no data
		if (count($this->data) == 0)
		{
			return false;
		}
		foreach ($this->rules as $rule)
		{
			//If we require the file we must first check if it's been submitted
			if (in_array('required', $rule['rules']) && !isset($this->data[$rule['field']]))
			{
				$this->errors[] = $this->_build_error_message($rule['label'], 'required');
				unset($this->rules['rules']['required']);
				return false;
			}
			else if (in_array('file_required', $rule['rules']) && !isset($_FILES[$rule['field']]))
			{
				$this->errors[] = $this->_build_error_message($rule['label'], 'file_required');
				unset($this->rules['rules']['file_required']);
				return false;
			}

			//If not required we procceed with the validations
			if (isset($this->data[$rule['field']]))
			{
				if (!$this->_validate($this->data[$rule['field']], $rule['rules'], $rule['label']))
				{
					return false;
				}
			}
		}
		return true;
	}

	protected function _validate(&$value, $rules, $label)
	{
		foreach ($rules as $rule)
		{
			//TODO: Proccess parameters
			$params = array();

			if (!method_exists($this, $rule))
			{
				continue;
			}
			if ($this->$rule($value, $params) === false)
			{
				//TODO: Add parameters
				$this->errors[] = $this->_build_error_message($label, $rule);
				return false;
			}
		}
		return true;
	}

	protected function _build_error_message($label, $rule, array $params = array())
	{
		//TODO: Add parameters
		$error_line = Lang::line("form_validation_{$rule}", array('field' => $label));

		if ($error_line !== null)
		{
			$this->errors[] = $error_line;
		}
		else
		{
			throw new \Exception("Unable to find error message for rule: {$rule}");
		}
	}

	//Error Output
	public function validation_errors($field = null)
	{
		if (empty($this->errors))
		{
			return null;
		}
		//TODO: Return error message from single input
		return implode('<br>', $this->errors);
	}

	//---------------------------------------------------------------------------------------------------
	//	Validation Functions
	//---------------------------------------------------------------------------------------------------
	public function matches($field, $label, $value, $label2)
	{
		return $field === $value;
	}
	
	//Data Preparation Functions
	public function xss_clean($field, $params)
	{
		
	}

	//Numeric Validation
	public function numeric($value, $params = null)
	{
		return is_numeric($value);
	}

	public function integer(&$value, $params = null)
	{
		if (ctype_digit($value))
		{
			$value = (int)$value;
			return true;
		}
		return false;
	}
	
	public function greater_than($field, $value)
	{
		return $this->numeric($field) && ((int)$field) < $value;
	}
	
	public function less_than($field, $value)
	{
		return $this->numeric($field) && ((int)$field) > $value;
	}
	
	//String Validation	
	public function alpha($field, $params)
	{
		return preg_match("/^([a-z])+$/i", $field);
	}
	
	public function alpha_numeric($field, $params)
	{
		return preg_match("/^([a-z0-9])+$/i", $field);
	}
	
	public function alpha_dash($field, $params)
	{
		return preg_match("/^([-a-z0-9_-])+$/i", $field);
	}
	
	public function min_length($field, $length)
	{
		return strlen($field) >= $length;
	}
	
	public function max_length($field, $length)
	{
		return strlen($field) <= $length;
	}
	
	public function exact_length($field, $length)
	{
		return strlen($field) === $length;
	}
	
	//Date Validation
	private $_standard_date_format = 'Y-m-d';

	public function valid_date($value, $format)
	{
		if (is_null($format) or $format === FALSE)
		{
			$format = $this->_standard_date_format;
		}
		if (function_exists ('date_parse_from_format'))
		{
			$parsed = date_parse_from_format($format, $value);
		}
		else
		{
			$parsed = $this->_date_parse_from_format($format, $value);
		}
		if($parsed['warning_count'] > 0 or $parsed['error_count'] > 0)
		{
			return false;
		}
		return true;
	}

	public function is_date($date_string, $label)
	{
		return DateTime::createFromFormat('Y-m-d', $date_string);
	}

	public function is_date_time($date_string, $label)
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $date_string);
	}
	
	public function date_before($date_string, $label, $value)
	{
		
	}
	
	public function date_after($date_string, $label, $value)
	{
	
	}
	
	//Other Validations
	public function is_email($field, $params = null)
	{
		return filter_var($field, FILTER_VALIDATE_EMAIL) !== false;
	}

	//TODO: Add in_database and file validation methods
	public function exists_in_db($str, $field)
	{		
		list($table, $field)=explode('.', $field);
		/*
		$query = $this->CI->db->limit(1)->get_where($table, array($field => $str));

		if ($query->num_rows() === 0)
		{
			$this->set_message('exists_in_db', "%s no es un valor válido.");
			return false;
		}
		return true;
		*/
	}
}
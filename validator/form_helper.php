<?php

function form_open($action = null, $attributes = null)
{
	if ($action === null)
	{
		$action = current_url();
	}
	else
	{
		$action = site_url($action);
	}
	$defaults = ['action' => $action, 'method' => 'post'];
	return '<form ' . _parse_html_attributes($attributes, $defaults) . '>';
}

function form_open_multipart($action = '', $attributes = null)
{
	if ($action === null)
	{
		$action = explode("?", $_SERVER['REQUEST_URI'])[0];
	}
	else
	{
		$action = site_url($action);
	}
	$defaults = ['action' => $action, 'method' => 'post'];
	return '<form ' . _parse_html_attributes($attributes, $defaults) . '>';
}

function form_input($name = '', $value = '', $attributes = '')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name,
		'value' => isset($_POST[$name]) ? $_POST[$name] : $value,
		'type' => 'text'
	);

	return '<input '._parse_html_attributes($attributes, $defaults).'/>'.PHP_EOL;
}

function form_password($name = '', $value = '', $attributes = '')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name,
		'value' => isset($_POST[$name]) ? $_POST[$name] : $value,
		'type' => 'password'
	);

	return '<input '._parse_html_attributes($attributes, $defaults).'/>';
}

function form_upload($name = '', $attributes = '')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name,
		'type' => 'file'
	);

	return '<input '._parse_html_attributes($attributes, $defaults).'/>';
}

function form_hidden($name = '', $value = '', $attributes = '')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name,
		'value' => isset($_POST[$name]) ? $_POST[$name] : $value,
		'type' => 'hidden'
	);

	return '<input '._parse_html_attributes($attributes, $defaults).'/>';
}

function form_textarea($name = '', $value = '', $attributes = '')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name
	);

	return '<textarea '._parse_html_attributes($attributes, $defaults).'>'.set_value($name, $value).'</textarea>'.PHP_EOL;
}

function form_dropdown($name = '', $options = array(), $selected = array(), $attributes = '')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name
	);
	$selected = isset($_POST[$name]) ? $_POST[$name] : $selected;
	if (!is_array($selected))
	{
		$selected = [$selected];
	}
	$select = '<select '._parse_html_attributes($attributes, $defaults).'>';
	if (is_assoc($options))
	{
		foreach ($options as $value => $display)
		{
			$select .= PHP_TAB.'<option value="'. $value . '"' . (!empty($value) && in_array($value, $selected) ? ' selected' : '') .'>' . htmlspecialchars($display) . '</option>'.PHP_EOL;
		}
	}
	else
	{
		foreach ($options as $option)
		{
			$select .= PHP_TAB.'<option value="'. $option . '"' . (!empty($option) && in_array($option, $selected) ? ' selected' : '') .'>' . htmlspecialchars($option) . '</option>'.PHP_EOL;
		}
	}
	$select .= '</select>'.PHP_EOL;
	return $select;
}

function form_dropdown_from_objects($name = '', $options = array(), $selected = array(), $attributes = '', $value_property = 'value', $display_property = 'name')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name
	);

	$selected = isset($_POST[$name]) ? $_POST[$name] : $selected;
	if (!is_array($selected))
	{
		$selected = [$selected];
	}

	$select = '<select '._parse_html_attributes($attributes, $defaults).'>'.PHP_EOL;
	foreach ($options as $option)
	{
		$select .= PHP_TAB.'<option value="'. $option->$value_property . '"' . (!empty($option->$value_property) && in_array($option->$value_property, $selected) ? ' selected' : '') .'>' . htmlspecialchars($option->$display_property) . '</option>'.PHP_EOL;
	}
	$select .= '</select>'.PHP_EOL;
	return $select;
}

function form_multiselect()
{

}

function form_checkbox($name, $value = '', $checked = false, $attributes = '')
{
	$defaults = array(
		'name' => is_array($name) ? '' : $name,
		'value' => isset($_POST[$name]) ? $_POST[$name] : $value,
		'type' => 'checkbox'
	);

	return '<input '._parse_html_attributes($attributes, $defaults).'/>';
}

function form_radio()
{

}

function _parse_html_attributes($attributes, $defaults)
{
	$attr_str = '';
	if (is_array($attributes))
	{
		foreach ($defaults as $key => $val)
		{
			if (isset($attributes[$key]))
			{
				$defaults[$key] = $attributes[$key];
				unset($attributes[$key]);
			}
		}

		if (count($attributes) > 0)
		{
			$defaults = array_merge($defaults, $attributes);
		}
	}

	foreach ($defaults as $key => $val)
	{
		if ($key === 'value')
		{
			$val = html_escape($val);
		}
		elseif ($key === 'name' && ! strlen($defaults['name']))
		{
			continue;
		}

		$attr_str .= $key.'="'.$val.'" ';
	}

	if (is_string($attributes))
	{
		$attr_str .= trim($attributes).' ';
	}

	return $attr_str;
}

function set_value($field, $default = '', $html_escape = TRUE)
{
	$value = isset($_POST[$field]) ? $_POST[$field] : $default;
	return ($html_escape) ? html_escape($value) : $value;
}

function set_checkbox($field, $value = '', $default = FALSE)
{
	$CI =& get_instance();

	if (isset($CI->form_validation) && is_object($CI->form_validation) && $CI->form_validation->has_rule($field))
	{
		return $CI->form_validation->set_checkbox($field, $value, $default);
	}
	elseif (($input = $CI->input->post($field, FALSE)) === NULL)
	{
		return ($default === TRUE) ? ' checked="checked"' : '';
	}

	$value = (string) $value;
	if (is_array($input))
	{
		// Note: in_array('', array(0)) returns TRUE, do not use it
		foreach ($input as &$v)
		{
			if ($value === $v)
			{
				return ' checked="checked"';
			}
		}

		return '';
	}

	return ($input === $value) ? ' checked="checked"' : '';
}

function set_radio()
{

}

function set_select()
{

}

function form_error()
{

}

function validation_errors()
{

}

function form_prep()
{

}

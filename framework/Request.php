<?php

namespace Star\Framework;

class Request
{
	const POST = 'POST';
	const GET = 'GET';
	const PUT = 'PUT';
	const HEAD = 'HEAD';
	const DELETE = 'DELETE';
	const OPTIONS = 'OPTIONS';

	private static $headers = [];
	
	public static function headers($header_key = null)
	{
		if (empty(self::$headers))
		{
			// In Apache, you can simply call apache_request_headers()
			if (function_exists('apache_request_headers'))
			{
				self::$headers = apache_request_headers();
			}
			else
			{
				isset($_SERVER['CONTENT_TYPE']) && self::$headers['Content-Type'] = $_SERVER['CONTENT_TYPE'];
	
				foreach ($_SERVER as $key => $val)
				{
					if (sscanf($key, 'HTTP_%s', $header) === 1)
					{
						// take SOME_HEADER and turn it into Some-Header
						$header = str_replace('_', ' ', strtolower($header));
						$header = str_replace(' ', '-', ucwords($header));
	
						self::$headers[$header] = $_SERVER[$key];
					}
				}
			}
		}

		if (is_string($header_key))
		{
			return isset(self::$headers[$header_key]) ? self::$headers[$header_key] : null;
		}

		return self::$headers;
	}

	public static function is_ajax()
	{
		return !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
	}

	public static function referrer()
	{
		if (isset($_SERVER['HTTP_REFERER']))
		{
			return $_SERVER['HTTP_REFERER'];
		}
		return null;
	}

	public static function ip_address()
	{
		return $_SERVER['REMOTE_ADDR'];
	}

	public static function valid_ip($which = 'ipv4')
	{
		switch (strtolower($which))
		{
			case 'ipv4':
				$which = FILTER_FLAG_IPV4;
				break;
			case 'ipv6':
				$which = FILTER_FLAG_IPV6;
				break;
			default:
				$which = NULL;
				break;
		}

		return (bool) filter_var($ip, FILTER_VALIDATE_IP, $which);
	}

	public static function agent()
	{
		if (isset($_SERVER['HTTP_USER_AGENT']))
		{
			return $_SERVER['HTTP_USER_AGENT'];
		}
		return null;
	}

	public static function is_cli_request()
	{
		return (PHP_SAPI === 'cli' OR defined('STDIN'));
	}

	public static function method()
	{
		return strtoupper($_SERVER['REQUEST_METHOD']);
	}

	public static function is_method($method)
	{
		return $_SERVER['REQUEST_METHOD'] === $method;
	}

	public static function server($key)
	{
		if (isset($_SERVER[$key]))
		{
			return $_SERVER[$key];
		}
		return null;
	}

	//If over HTTTPS
	public static function is_secure()
	{
		if (!empty($_SERVER['https']))
            return true;

        if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
            return true;

        return false;
	}

	public static function is_json()
	{
		$content_type = self::headers('Content-Type');
		return $content_type !== null && strpos('application/json', $content_type) !== false;
	}

	public static function request_json()
	{
		$accepts = self::headers('Accept');
		return $accepts !== null && strpos('application/json', $accepts) !== false;
	}
}

<?php

namespace Star\Framework;

use Star\Framework\Config;
use Star\Framework\Lang;
use Star\Framework\Router;
use Psr\Log\LoggerInterface;
use Monolog\Logger;

//Load the global functions
require_once __DIR__ . '/core/functions.php';

class Application
{
	public $router;

	public $logger;

	private static $instance = null;

	public static function get_instance()
	{
		return self::$instance;
	}

	public function __construct($app_path)
	{
		self::$instance = & $this;
		$app_path = rtrim($app_path, '/') . '/';
		define('APP_PATH', $app_path);
		define('BASE_PATH', dirname($app_path) . '/');
		$protocol = (isset($_SERVER['HTTPS'])) ? (($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != 'off') ? 'https' : 'http') : 'http';
		$base_url = $protocol.'://'.$_SERVER['HTTP_HOST'].'/';
		$path_relative_to_root = str_replace($_SERVER['DOCUMENT_ROOT'], '', dirname($_SERVER['SCRIPT_FILENAME']));
		if ($path_relative_to_root !== '')
		{
			$path_relative_to_root .= '/';
		}
		$base_url .= ltrim($path_relative_to_root, "/");
		define('BASE_URL', $base_url);
		Config::init();		
		date_default_timezone_set(Config::app_config('server_timezone'));
		$this->router = new Router();
		Lang::set_language(Config::app_config('default_language'));
		$index_page = Config::app_config('index_page');
		$site_url = (!empty($index_page)) ? BASE_URL . $index_page . '/' : BASE_URL;
		define('SITE_URL', $site_url);
		$this->logger = new Logger('app_logger');
		$this->logger->pushHandler(new \Monolog\Handler\StreamHandler(APP_PATH.'logs/mibeca.log', Logger::DEBUG));
		//log_info('My logger is now ready');
	}

	public function run()
	{
		$this->router->init();
		$this->router->run();
	}
}

<?php

namespace Star\Framework;

class Upload
{
	private $config = array(
				'upload_path' => '',
				'allowed_types' => 'gif|jpg|png',
				'max_size' => 2048,
				'file_name' => null,
				'overwrite' => false
			);

	private $errors = array();

	private $data = array();

	public function __construct($config = null)
	{
		if ($config !== null)
		{
			$this->initialize($config);
		}
	}

	public function initialize(array $config = array(), $reset = TRUE)
	{
		/*Config keys: -image (bool): wether to validate if it's image
						-max_width, max_height, min_width, min_height
						-allowed_extensions (array): array of extensions allowed
						-image_type (int/ENUM): one of the IMAGETYPE_XXX constants
						-encrypt_name (bool): wether to generate random name(s) for the file(s)
						-remove_spaces
						-name_pattern (string or function?): [TODO] an expression for new filename, can use index, extension, original filename
		*/
		$this->config = array_merge($this->config, $config);
	}

	public function do_upload($field, array $options = array())
	{
		if (!isset($_FILES[$field]))
		{
			return false;
		}
		$this->config = array_merge($this->config, $options);
		if (isset($_FILES[$field]['name'][0]))
		{
			$files = $_FILES;
			$length = count($_FILES[$field]['name']);
			$result = true;

			//Hack to proccess file array
			for ($i = 0; $i < $length; $i++)
			{
				$_FILES[$field]['name'] = $files[$field]['name'][$i];
				$_FILES[$field]['type'] = $files[$field]['type'][$i];
				$_FILES[$field]['tmp_name'] = $files[$field]['tmp_name'][$i];
				$_FILES[$field]['error'] = $files[$field]['error'][$i];
				$_FILES[$field]['size'] = $files[$field]['size'][$i];
				$file = $_FILES[$field];
				
				if ($file['error'] === UPLOAD_ERR_NO_FILE)
				{
					continue;
				}
				
				$uploaded = $this->upload_file($file, $i);
				$result = $result && $uploaded;
			}
			return $result;
		}
		else
		{
			$file = $_FILES[$field];
			return $this->upload_file($file);
		}
	}

	//TODO: Validate filaneme rules correctly (only validate overwrite when new name provided, etc)
	private function upload_file($file, $index = null)
	{
		$original_name = basename($file['name']);
		$extension = substr(strrchr($original_name, '.'), 1);
		$target_path = rtrim($this->config['upload_path'], DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
		
		if (isset($this->config['name_pattern']))
		{
			$filename = str_replace(array('{index}'), array($index), $this->config['name_pattern']);
		}	
		else if (isset($this->config['encrypt_name']) && $this->config['encrypt_name'] === true)
		{
			$filename = md5(uniqid(mt_rand())).$extension;
			while (file_exists($target_path . $filename) && !$this->config['overwrite'])
			{
				$filename = md5(uniqid(mt_rand())).$extension;
			}
		}
		else
		{
			$filename = ($this->config['file_name'] !== null) ? $this->config['file_name']  :basename($file['name']);
		}
		if (isset($this->config['remove_spaces']) && $this->config['remove_spaces'])
		{
			$filename = str_replace(' ', '_', $filename);
		}
		$target_file = $target_path . $filename;

		//Validation
		$is_image = getimagesize($file['tmp_name']);
		if ($is_image !== false && $this->config['image'])
		{
			//Validate image according to options...
			if (!$this->validate_image($is_image))
			{
				return false;
			}
		}
		else
		{
			$this->set_error("File: \"{$original_name}\" must be an image.");
			return false;
		}
		if (file_exists($target_file) && !$this->config['overwrite'])
		{
			$this->set_error("Cannot upload file: \"{$original_name}\", file already exists");
			return false;
		}
		if ($file['size'] > $this->config['max_size'])
		{
			$this->set_error("File: \"{$original_name}\" exceeds the {$this->config['max_size']} bytes maximum.");
			return false;
		}
		if (isset($this->config['allowed_extensions']))
		{
			if (!in_array($extension, $this->config['allowed_extensions']))
			{				
				$this->set_error("File: \"{$original_name}\" doesn't have an accepted extension.");
				return false;
			}
		}

		if ($file['error'] !== UPLOAD_ERR_OK)
		{
			switch ($file['error'])
			{
				case UPLOAD_ERR_INI_SIZE:
					$this->set_error('upload_file_exceeds_limit');
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$this->set_error('upload_file_exceeds_form_limit');
					break;
				case UPLOAD_ERR_PARTIAL:
					$this->set_error('upload_file_partial');
					break;
				case UPLOAD_ERR_NO_FILE:
					$this->set_error('upload_no_file_selected');
					break;
				case UPLOAD_ERR_NO_TMP_DIR:
					$this->set_error('upload_no_temp_directory');
					break;
				case UPLOAD_ERR_CANT_WRITE:
					$this->set_error('upload_unable_to_write_file');
					break;
				case UPLOAD_ERR_EXTENSION:
					$this->set_error('upload_stopped_by_extension');
					break;
			}
			return false;
		}

		if (move_uploaded_file($file['tmp_name'], $target_file))
		{
			$finfo = new finfo(FILEINFO_MIME, $target_file);

			$width = $height = $type = null;
			if ($is_image !== false)
			{
				list($width, $height, $type, $attr) = $image_data;
			}
			$data = array(
				'file_name'		=> $filename,
				'file_type'		=> $file['type'],
				'file_path'		=> $target_path,
				'full_path'		=> $target_file,
				'raw_name'		=> str_replace($extension, '', $filename),
				'orig_name'		=> $original_name,
				//'client_name'	=> $this->client_name,
				'file_ext'		=> $extension,
				'file_size'		=> $file['size'],
				'is_image'		=> $is_image !== false,
				'image_width'	=> $width,
				'image_height'	=> $height,
				'image_type'	=> $type//,
				//'image_size_str'=> $this->image_size_str,
			);
			$this->data[] = $data;
			return true;
		}
		else
		{
			$this->set_error('unknown_error');
			return false;
		}
	}

	//Function to be called by upload_file and by Form_validation
	public function validate_file($file)
	{

	}

	private function validate_image($image_data)
	{
		list($width, $height, $type, $attr) = $image_data;

		if (isset($this->config['image_type']) && $this->config['image_type'] !== $type)
		{
			$this->set_error('File type not allowed.');
			return false;
		}
		if (isset($this->config['max_width']) && $width > $this->config['max_width'])
		{
			$this->set_error("Width must be lower than {$this->config['max_width']} pixels.");
			return false;
		}
		if (isset($this->config['max_height']) && $height > $this->config['max_height'])
		{
			$this->set_error("Height must be lower than {$this->config['max_height']} pixels.");
			return false;
		}
		if (isset($this->config['min_width']) && $width < $this->config['min_width'])
		{
			$this->set_error("Width must be higher than {$this->config['min_width']} pixels.");
			return false;
		}
		if (isset($this->config['min_height']) && $height > $this->config['min_height'])
		{
			$this->set_error("Height must be higher than {$this->config['min_width']} pixels.");
			return false;
		}
		return true;
	}

	private function set_error($message)
	{
		echo $message;
	}

	//TODO: Return data from single input file
	public function data($index = null)
	{
		if (empty($this->data))
		{
			return null;
		}

		if (!empty($index))
		{
			return isset($data[$index]) ? $data[$index] : null;
		}
		return $this->data;
	}

	public function display_errors($open = '<p>', $close = '</p>')
	{
		if (empty($this->errors))
		{
			return null;
		}
		//TODO: Return error message from single input
		return ltrim(rtrim(implode($close.$open, $this->errors), $open), $close);
	}
}
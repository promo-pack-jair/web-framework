<?php

use Psr\Log\LogLevel;
use Star\Framework\Application;
use Star\Framework\Lang;

//---------------------------------------------------------------------------------------------------
//	Core Helper
//---------------------------------------------------------------------------------------------------

function base_url($path = '')
{
	return BASE_URL . $path;
}

function base_path($path = '')
{
	return BASE_PATH . $path;
}

function current_url()
{
	return (isset($_SERVER['HTTPS']) ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
}

function site_url($path = '')
{
	return SITE_URL . $path;
}

function app_path($path = '')
{
	return APP_PATH . $path;
}

function redirect($uri = '')
{
	$uri = site_url($uri);
	header("Location: {$uri}");
	exit;
}

function show_404()
{
	exit_system_error("The page you have requested couldn't be found.", 'Page not found', \HttpStatus::NOT_FOUND);
}

//Used to exit in case of system error
function exit_system_error($error, $error_tittle = 'Error', $status_code = \HttpStatus::INTERNAL_SERVER_ERROR)
{
	if ($status_code !== null)
	{
		http_response_code($status_code);
	}
	include(__DIR__ . '/../views/exit_error.php');
	exit;
}

//Checks if provided variable is an (associative) array
//(High Performance)
function is_assoc($array)
{
	foreach (array_keys($array) as $k => $v)
	{
		if ($k !== $v)
			return true;
	}
	return false;
}

function html_escape($var, $double_encode = TRUE)
{
	if (empty($var))
	{
		return $var;
	}
	if (is_array($var))
	{
		foreach (array_keys($var) as $key)
		{
			$var[$key] = html_escape($var[$key]);
		}
		return $var;
	}
	return htmlspecialchars($var, ENT_QUOTES);
}

function remove_invisible_characters($str, $url_encoded = TRUE)
{
	$non_displayables = array();

	// every control character except newline (dec 10),
	// carriage return (dec 13) and horizontal tab (dec 09)
	if ($url_encoded)
	{
		$non_displayables[] = '/%0[0-8bcef]/i';	// url encoded 00-08, 11, 12, 14, 15
		$non_displayables[] = '/%1[0-9a-f]/i';	// url encoded 16-31
		$non_displayables[] = '/%7f/i';	// url encoded 127
	}

	$non_displayables[] = '/[\x00-\x08\x0B\x0C\x0E-\x1F\x7F]+/S';	// 00-08, 11, 12, 14-31, 127

	do
	{
		$str = preg_replace($non_displayables, '', $str, -1, $count);
	}
	while ($count);

	return $str;
}

function camel_case_to_snake_case($str)
{
	return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $str)), '_');
}

// Helper Methods

function lang($index, array $params = array())
{
	return Lang::line($index, $params);
}

function log_message($level, $message)
{
	Application::get_instance()->logger->log($level, $message);
}

function log_emergency($message, array $context = array())
{
	log_message(LogLevel::EMERGENCY, $message, $context);
}

function log_alert($message, array $context = array())
{
	log_message(LogLevel::ALERT, $message, $context);
}

function log_critical($message, array $context = array())
{
	log_message(LogLevel::CRITICAL, $message, $context);
}

function log_error($message, array $context = array())
{
	log_message(LogLevel::ERROR, $message, $context);
}

function log_warning($message, array $context = array())
{
	log_message(LogLevel::WARNING, $message, $context);
}

function log_notice($message, array $context = array())
{
	log_message(LogLevel::NOTICE, $message, $context);
}

function log_info($message, array $context = array())
{
	log_message(LogLevel::INFO, $message, $context);
}

function log_debug($message, array $context = array())
{
	log_message(LogLevel::DEBUG, $message, $context);
}

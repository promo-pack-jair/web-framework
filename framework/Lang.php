<?php

namespace Star\Framework;

//TODO: Lang superglobal or global functions for access in views
//TODO: Support to pass parameters to lines
class Lang
{
	private static $default_language;
	private static $loaded = array();
	private static $lines = array();

	public static function set_language($language)
	{
		self::$default_language = $language;
	}

	public static function load($file, $language = null)
	{
		if ($language === null)
		{
			$language = self::$default_language;	
		}

		if (in_array($file, self::$loaded, TRUE))
		{
			return;
		}
		if (file_exists($file_path = APP_PATH."lang/{$language}/{$file}.php") || file_exists($file_path = APP_PATH."lang/{$language}/{$file}_lang.php"))
		{
			include_once($file_path);
		}
		else
		{
			exit_system_error("Couldn't find language file: \"{$language}/{$file}.php\"");
		}

		if (!isset($lang) OR ! is_array($lang))
		{
			exit_system_error("The file: \"{$language}/{$file}.php\" doesn't seem to contain a valid language array.");
		}
		self::$lines = array_merge(self::$lines, $lang);
		self::$loaded[] = $file;
	}

	public static function line($index, array $params = array())
	{
		if (!isset(self::$lines[$index]))
		{
			return null;
		}
		$line = self::$lines[$index];
		if ($params)
		{
			$line = self::sprintf2($line, $params);
		}
		return $line;
	}

	//Modified from: http://php.net/manual/en/function.sprintf.php#81706
	private static function sprintf2($str, $vars = array())
	{
		if (!$str) return '';
		if (count($vars) > 0)
		{
			foreach ($vars as $k => $v)
			{
				$str = str_replace('{' . $k . '}', $v, $str);
			}
		}

		return $str;
	}
}

<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<title>Error</title>
	</head>
	
	<body style="color:#4F5155;font:13px/20px normal Helvetica,Arial,sans-serif;margin:40px;">
		<div style="border:1px solid #D0D0D0;margin-top:25px;">
			<h1 style="border-bottom:1px solid #D0D0D0;font-weight:normal;margin:0 0 14px;padding:14px;font-size:19px;"><?= $error_tittle; ?></h1>
			<p style="padding-left:14px;"><?= $error; ?></p>
		</div>
	</body>
</html>

<?php

namespace Star\Framework;

class Config
{
	private static $app_config = array();
	private static $loaded = array();
	private static $config = array();

	public static function init()
	{
		if (file_exists($file_path = APP_PATH.'config/app.php'))
		{
			include_once($file_path);
			self::$app_config = $app;
		}
		else
		{
			throw new \Exception("Couldn't find configuration file config/app.php");
		}
		if (file_exists($file_path = APP_PATH.'config/constants.php'))
		{
			include_once($file_path);
		}
		else
		{
			throw new \Exception("Couldn't find configuration file config/constants.php");
		}
	}

	public static function app_config($key)
	{
		return self::$app_config[$key];
	}

	public static function load($file, $array_name = null, $return = false)
	{
		if (in_array($file, self::$loaded, TRUE))
		{
			return;
		}
		if (file_exists($file_path = APP_PATH."config/{$file}.php"))
		{
			include($file_path);
		}
		else
		{
			exit_system_error("Couldn't find configuration file: \"{$file}.php\"");
		}

		if ($array_name === null && (!isset($config) OR !is_array($config)))
		{
			exit_system_error("The file: \"{$file}.php\" doesn't seem to contain a valid configuration array.");
		}
		if ($return)
		{
			return isset($config) ? $config : $$array_name;
		}
		if ($array_name !== null)
		{
			self::$config = array_merge(self::$config, $$array_name);
		}
		else
		{
			self::$config = array_merge(self::$config, $config);
		}
		self::$loaded[] = $file;
	}

	public static function item($item)
	{
		return isset(self::$config[$item]) ? self::$config[$item] : null;
	}
}

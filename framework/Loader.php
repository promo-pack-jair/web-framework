<?php

namespace Star\Framework;

use Star\Framework\Config;

class Loader
{
	protected $controller;
	protected $cached_vars = array();

	function __construct($parent)
	{
		$this->controller = & $parent;
	}

	public function view($filename, array $data = array(), $return = false)
	{
		if ($end = strrpos($filename, ".php"))
		{
			$filename = substr($filename, 0, $end);
		}
		$this->get_vars();
		extract($data);
		if ($return)
		{
			ob_start();
			include app_path("views/{$filename}.php");
			return ob_get_clean();
		}
		else
		{
			include app_path("views/{$filename}.php");
		}
	}

	public function library($filename, $config = array(), $field_name = null)
	{
		if ($field_name === null)
		{
			$field_name = explode('/', $filename);
			$field_name = end($field_name);
			$field_name = camel_case_to_snake_case($field_name);
		}
		if (isset($this->controller->$field_name))
		{
			return;
		}
		if ($filename === 'email')
		{
			if (empty($config))
			{
				$config = \Star\Framework\Config::load('email', 'email', true);
			}
			$mail = new \PHPMailer\PHPMailer\PHPMailer();
			if ($config['protocol'] === 'smtp')
			{
				$mail->isSMTP();
				//$mail->SMTPDebug = 2;
				$mail->Host = $config['smtp']['host'];  // Specify main and backup SMTP servers
				$mail->SMTPAuth = false;                               // Enable SMTP authentication
				$mail->Username = $config['smtp']['user'];                 // SMTP username
				$mail->Password = $config['smtp']['pass'];
				$mail->Port = $config['smtp']['port'];
			}
			if ($config['mailtype'] === 'html')
			{
				$mail->isHTML(true);
			}
			$this->controller->email = $mail;
			return;
		}

		//TODO: Support for core system libraries loading and overriding
		include_once app_path("libraries/{$filename}.php");

		if (is_null($filename) || !is_string($filename))
		{
			throw new Exception("Filename must be a string.");
		}

		if (!is_null($field_name) && !is_string($field_name))
		{
			throw new Exception("Provided Field Name must be a string.");
		}

		if (!is_null($config))
		{
			$this->controller->$field_name = new $filename($config);
		}
		else
		{
			$this->controller->$field_name = new $filename;
		}
	}

	public function model($filename)
	{
		include_once(app_path("models/{$filename}.php"));
		$class = Config::app_config('project_namespace').'\models\\'.str_replace('/', '\\', $filename);
		$property = explode('/', $filename);
		$property = end($property);
		$property = camel_case_to_snake_case($property);
		$this->controller->$property = new $class;
	}

	public function helper($filename)
	{
		if (file_exists($file_path = app_path("helpers/{$filename}.php")))
		{
			include_once $file_path;
		}
		//Include from framework folder
		/*else if (file_exists($file_path = __DIR__ . "../helpers/{$filename}.php"))
		{
			include_once $file_path;
		}*/
		else
		{
			throw new \Exception("Couldn't find helper file: \"{$file_path}\"");
		}
	}

	private function get_vars()
	{
		$controller = & $this->controller;
		$vars = get_object_vars($controller);
		foreach ($vars as $key => $var)
		{
			if (!isset($this->$key))
				$this->$key = & $this->controller->$key;
		}
		extract($this->cached_vars);
	}
}

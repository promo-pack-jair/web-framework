<?php

namespace Star\Framework;

use Star\Framework\Config;
use Star\Framework\Request;

class Router
{
	public $directory		= '';
	public $directory_uc	= '';
	public $class			= '';
	public $class_namespace	= '';
	public $action			= '';
	public $params			= array();
	public $controller_namespace = '';
	public $uri;
	public $routed_uri;

	private $controller_reflector;
	private $registered_routes;
	private $override_404;
	protected static $route_symbols = array(':num', ':any');

	function __construct()
	{
		$this->base_url = Config::app_config('base_url');
		$this->index_page = Config::app_config('index_page');
		$this->registered_routes = $routes = Config::load('routes', 'routes', true);
		$this->controller_namespace = Config::app_config('project_namespace') . '\\Controllers\\';
		$this->class = $routes['default_controller'];
		$this->action = $routes['default_action'];
		$this->override_404 = $this->registered_routes['404_override'];
		unset($this->registered_routes['default_controller']);
		unset($this->registered_routes['default_action']);
		unset($this->registered_routes['404_override']);
	}

	public function init(array $options = array())
	{
		if (empty($options))
		{
			$this->get_request_uri($_SERVER['REQUEST_URI']);
			$this->proccess_custom_routes($this->uri);
			if (empty($this->class_namespace))
			{
				$this->parse_uri();
			}
		}
		else if (isset($options['uri']))
		{
			$this->uri = $options['uri'];
			$this->proccess_custom_routes($this->uri);
			if (empty($this->class_namespace))
			{
				$this->parse_uri($this->uri);
			}
		}
		else
		{
			if (isset($options['controller']))
			{
				$this->set_controller($options['controller']);
			}
			if (isset($options['action']))
			{
				$this->set_action($options['action']);
			}
			if (isset($options['params']))
			{
				$this->set_params($options['params']);
			}
		}
	}
	
	public function get_request_uri($request_uri)
	{
		$uri = parse_url($request_uri);
		$uri = isset($uri['path']) ? $uri['path'] : '';
		$uri = rtrim(ltrim($uri, '/'), '/');
		if (isset($_SERVER['SCRIPT_NAME']))
		{
			$script_name = ltrim($_SERVER['SCRIPT_NAME'], '/');
			if (!empty($uri) && strpos($script_name, $uri) === 0)
			{
				$uri = substr($script_name, strlen($uri));
			}
			else if (strpos($uri, $script_name = dirname($script_name)) === 0)
			{
				$uri = substr($uri, strlen($script_name));
			}
		}
		$uri = ltrim($uri, '/');
		//Remove front facing controller (default: index.php) from uri
		$uri = ltrim($uri, Config::app_config('index_page'));
		$uri = ltrim($uri, '/');			
		$this->uri = $uri;
	}

	public function set_default_controller($controller)
	{
		$this->class = $controller;
	}

	public function parse_uri($uri = null)
	{
		$uri_components = explode('/', $uri === null ? $this->uri : $uri);
		if (count($uri_components) > 0)
		{
			while (isset($uri_components[0]) && is_dir(APP_PATH . "controllers/{$uri_components[0]}"))
			{
				$this->directory .= "{$uri_components[0]}/";
				$this->directory_uc .= ucfirst($uri_components[0]) . "\\";
				array_shift($uri_components);
			}
			$this->directory = rtrim($this->directory, '/');
			@list($controller, $action, $params) = array_pad(explode('/', implode('/', $uri_components), 3), 3, null);
			$this->set_controller($controller);
			if (isset($action))
			{
				$this->set_action($action);
			}
			if (isset($params))
			{
				$this->set_params(explode('/', $params));
			}
		}
		return [
			'directory' => $this->directory,
			'controller' => $this->class,
			'action' => $this->action,
			'params' => $this->params
		];
	}

	public function set_controller($controller)
	{
		$controller = (empty($controller)) ? $this->class : str_replace('_', '', ucwords(str_replace('-', '_', strtolower($controller)), '_'));

		$this->class = $controller;
		if ($this->directory !== '')
		{
			$this->class_namespace = $this->controller_namespace . $this->directory_uc . $controller;
		}
		else
		{
			$this->class_namespace = $this->controller_namespace . $controller;
		}

		if (!class_exists($this->class_namespace))
		{
			$this->class_namespace = $this->class = null;
		}
	}

	public function set_action($action)
	{
		$this->action = (empty($action)) ? $this->action : str_replace('-', '_', $action);
		if (empty($this->class_namespace))
		{
			$this->action = null;
		}
		else
		{
			$this->controller_reflector = new \ReflectionClass($this->class_namespace);
			if (!$this->controller_reflector->hasMethod($this->action))
			{
				$this->class_namespace = $this->class = null;
			}
		}
	}

	public function set_params(array $params)
	{
		for ($i = 0, $ln = count($params); $i < $ln; $i++)
		{
			$params[$i] = urldecode($params[$i]);
		}
		$this->params = $params;
	}

	public function debug_info()
	{
		return [
			'uri' => $this->uri,
			'directory' => $this->directory,
			'controller' => $this->class,
			'action' => $this->action,
			'params' => $this->params
		];
	}

	public function run()
	{
		if (!Config::app_config('published'))
		{
			if (is_null(Config::app_config('unpublished_view')))
			{
				exit_system_error(Config::app_config('unpublished_message'), Config::app_config('unpublished_message_title'));
			}
			else
			{
				$filename = Config::app_config('unpublished_view');
				include app_path("views/{$filename}.php");
			}
			return;
		}
		if ($this->class === null)
		{
			$this->run_404();
			return;
		}
		$this->controller_reflector = new \ReflectionClass($this->class_namespace);
		$is_valid_controller = $this->controller_reflector->isInstantiable() && $this->controller_reflector->isSubclassOf('Star\Framework\Controller');
		if ($is_valid_controller)
		{
			$controller = new $this->class_namespace();
			call_user_func_array(array($controller, $this->action), $this->params);
		}
		else
		{
			throw new \InvalidArgumentException('Wrong Request');
		}
	}

	public function run_404()
	{
		if (!empty($this->override_404))
		{
			$this->uri = $this->override_404;
			$this->directory = $this->directory_uc = $this->override_404 = '';
			$this->parse_uri();
			$this->run();
		}
		else
		{
			show_404();
		}
	}

	private function proccess_custom_routes($request_uri)
	{
		$uri_segments = explode('/', $request_uri);
		foreach ($this->registered_routes as $route => $value)
		{
			$segments = explode('/', $route);

			if (count($uri_segments) === count($segments))
			{
				if ($route === $request_uri)
				{					
					if (is_array($value))
					{
						if (isset($value[Request::method()]))
						{
							$value = $value[Request::method()];
						}
						else
						{
							$this->run_404();
						}
					}
					$this->parse_uri($this->routed_uri = $value);
					break;
				}
				else
				{
					$params = [];
					for ($i = 0, $len = count($segments); $i < $len; $i++)
					{
						if ($segments[$i] === $uri_segments[$i])
						{
							continue;
						}
						else if (in_array($segments[$i], self::$route_symbols))
						{
							if ($segments[$i] === ':num' && ctype_digit($uri_segments[$i]))
							{
								$params[] = $uri_segments[$i];
							}
							else if ($segments[$i] === ':any')
							{
								$params[] = $uri_segments[$i];
							}
							else
							{
								continue 2;
							}
						}
						else
						{
							continue 2;
						}
					}
					if (is_array($value))
					{
						if (isset($value[Request::method()]))
						{
							$value = $value[Request::method()];
						}
						else
						{
							$this->run_404();
						}
					}
					for ($i = 0, $len = count($params); $i < $len; $i++)
					{
						$replace_index = '$' . ($i + 1);
						$value = str_replace($replace_index, $params[$i], $value);
					}
					$this->parse_uri($this->routed_uri = $value);
				}
			}
		}
	}
}

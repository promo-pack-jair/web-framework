<?php

namespace Star\Framework;

use Star\Framework\Config;

abstract class Controller
{
	public $load;

	public function __construct()
	{
		$app = Application::get_instance();
		$this->app = &$app;
		$this->router = &$app->router;
		$this->load = new Loader($this);
		$app->controller = &$this;

		Config::load('autoload');
		$autoload = Config::item('autoload');

		if (!empty($autoload['libraries']))
		{
			for ($i = 0, $length = count($autoload['libraries']); $i < $length; $i++)
			{
				if (!empty($autoload['libraries'][$i]))
				{
					$this->load->library($autoload['libraries'][$i]);
				}
			}
		}
		if (!empty($autoload['helpers']))
		{
			for ($i = 0, $length = count($autoload['helpers']); $i < $length; $i++)
			{
				if (!empty($autoload['helpers'][$i]))
				{
					$this->load->helper($autoload['helpers'][$i]);
				}
			}
		}
		if (!empty($autoload['models']))
		{
			for ($i = 0, $length = count($autoload['models']); $i < $length; $i++)
			{
				if (!empty($autoload['models'][$i]))
				{
					$this->load->model($autoload['models'][$i]);
				}
			}
		}
		if (!empty($autoload['config']))
		{
			for ($i = 0, $length = count($autoload['config']); $i < $length; $i++)
			{
				if (!empty($autoload['config'][$i]))
				{
					Config::load($autoload['config'][$i]);
				}
			}
		}
	}
}
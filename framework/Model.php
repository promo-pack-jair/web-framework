<?php
namespace Star\Framework;

abstract class Model
{
	protected $controller;
	protected $db;
	protected $table_name;
	
	//Save reference of the controller parent as parameter and inherit it's libraries
	//An optional parameter can be passed to reference a table
	function __construct($table_name = '')
	{
		$app = Application::get_instance();
		$this->controller = & $app->controller;
		if (!isset($this->controller->db))
		{
			$this->controller->db = new \Star\Database\DB();
		}
		$this->db = & $this->controller->db;
		if ($table_name !== '')
			$this->table_name = $table_name;
	}
	
	/**
	 * __get
	 *
	 * Allows models to access controller's loaded classes using the same syntax as controllers.
	 *
	 * @param	string
	 * @access private
	 */
	function __get($key)
	{
		return $this->controller->$key;
	}
}
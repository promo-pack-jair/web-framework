<?php

namespace Star\Session;

class Session
{
	const FLASH_DATA = 'flash';
	private static $values;
	private static $flash_values;

	public function __call($func, $args)
	{
		return forward_static_call_array(array('Session', $func), $args);
	}

	public function __set($key, $value)
	{
		$this->set_userdata($key, $value);
	}

	public function __get($key)
	{
		return $this->userdata($key);
	}

	public static function init()
	{
		session_start();
		self::$values =& $_SESSION;
		if (!isset(self::$values[self::FLASH_DATA]))
		{
			self::$values[self::FLASH_DATA] = array();
		}
		self::$flash_values =& self::$values[self::FLASH_DATA];
		foreach (self::$flash_values as $key => &$value)
		{
			if ($value['lifetime'] === 1)
			{
				$value['lifetime'] = 0;
			}
			else
			{
				unset(self::$flash_values[$key]);
			}
		}
	}

	public static function userdata($key, $value = null)
	{
		if (is_null($value) && $key !== self::FLASH_DATA)
		{
			if (isset(self::$values[$key]))
				return self::$values[$key];
			return null;
		}
		else
		{
			return self::set_userdata($key, $value);
		}
	}

	public static function set_userdata($key, $value)
	{
		return self::$values[$key] = $value;
	}

	public static function unset_userdata($key)
	{
		unset(self::$values[$key]);
	}

	public static function &all_userdata()
	{
		return self::$values;
	}

	public static function flashdata($key, $value = null)
	{
		if (is_null($value))
		{
			if (isset(self::$flash_values[$key]))
				return self::$flash_values[$key]['value'];
			return null;
		}
		else
		{
			self::set_flashdata($key, $value);
		}
	}

	public static function set_flashdata($key, $value)
	{
		self::$flash_values[$key] = array('value' => $value, 'lifetime' => 1);
	}

	public static function sess_destroy()
	{
		session_destroy();
	}
}

Session::init();
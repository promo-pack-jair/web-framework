<?php

namespace Star\Database\Drivers\MySQLi;

use Star\Database\DbResultInterface;

class MySQLiResult extends \MySQLi_Result implements DbResultInterface
{
	function fetch()
	{
		return $this->fetch_assoc();
	}

	function result()
	{
		return $this->fetch_all();
	}

	function num_rows()
	{
		return $this->num_rows;
	}

	function row()
	{
		if ($this->num_rows > 0)
		{
			return $this->fetch_object();
		}
		else
		{
			return null;
		}
	}

	function fetch_all($result_mode = null)
	{
		$rows = array();
		while ($row = $this->fetch_object())
		{
			$rows[] = $row;
		}
		return $rows;
	}
}

<?php

namespace Star\Database\Drivers\MySQLi;

use Star\Database\DbDriverInterface;

class MySQLiDriver extends \MySQLi implements DbDriverInterface
{
	public $queries = [];
	public $last_query = null;

	function query($query, $result_mode = null)
	{
		$this->last_query = $queries[] = $query;
		$this->real_query($query);
		if (count($this->error_list) > 1)
		{
			exit_system_error($this->error, "Database Error");
		}
		return new MySQLiResult($this);
	}

	function escape_string($string)
	{
		if (is_bool($string))
			return (int)$string;
		else if (is_null($string))
			return 'NULL';
		return $this->real_escape_string($string);
	}

	function set_timezone($timezone)
	{
		$this->query("SET time_zone = '{$timezone}';");
	}
}

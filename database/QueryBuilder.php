<?php

namespace Star\Database;

class QueryBuilder
{
	public $select = [];
	private $from = '';
	private $joins = [];
	public $where = [];
	private $limit = null;
	private $offset = null;
	private $distinct = false;
	private $having = [];
	private $group_by = [];
	private $order_by = [];
	private $set= [];
	
	private $db_functions = array('NOW()');

	private $db;
	private $db_driver;

	public function __construct($db)
	{
		$this->db = $db;
		$this->db_driver = $this->db->get_db_driver();
	}

	public function reset_query()
	{
		$this->select = [];
		$this->from = '';
		$this->joins = [];
		$this->where = [];
		$this->limit = null;
		$this->offset = null;
		$this->distinct = false;
		$this->having = [];
		$this->group_by = [];
		$this->order_by = [];
		$this->set= [];
		return $this;
	}

	public function select($fields)
	{
		$this->select[] = $fields;
		return $this;
	}

	//select_avg([$select = ''[, $alias = '']]), select_max([$select = ''[, $alias = '']]), select_min([$select = ''[, $alias = '']]), select_sum([$select = ''[, $alias = '']]),
	
	public function distinct()
	{
		$this->distinct = true;
		return $this;
	}

	public function from($table)
	{
		$this->from = $table;
		return $this;
	}

	public function join($table, $on_clause, $join_type = '', $escape = true)
	{
		$this->joins[] = ['table' => $table, 'on' => $on_clause, 'join_type' => $join_type, 'escape' => $escape, 'where' => []];
		return $this;
	}
	
	public function where($where, $value = null, $escape = true)
	{
		$this->where[] = ['where' => $where, 'value' => $value];
		return $this;
	}
	
	//or_where($key[, $value = NULL[, $escape = NULL]]), or_where_in([$key = NULL[, $values = NULL[, $escape = NULL]]]), or_where_not_in([$key = NULL[, $values = NULL[, $escape = NULL]]]), where_in([$key = NULL[, $values = NULL[, $escape = NULL]]]),  where_not_in([$key = NULL[, $values = NULL[, $escape = NULL]]])

	public function group_by($by, $escape = true)
	{

	}

	public function order_by($order_by, $direction = '', $escape = true)
	{
		$this->order_by[] = ['field' => $order_by, 'direction' => $direction, 'escape' => $escape];
		return $this;
	}

	public function limit($value = 0, $offset = 0)
	{
		if ($value != 0)
		{
			$this->limit = $value;
		}
		if ($offset != 0)
		{
			$this->offset($offset);
		}
		return $this;
	}

	public function offset($offset)
	{
		if ($offset != 0)
		{
			$this->offset = $offset;
		}
		return $this;
	}

	public function set($key, $value = null, $escape = null)
	{
		$this->set[] = ['key' => $key, 'value' => $value, 'escape' => $escape];
		return $this;
	}

	//-------------------------------------------
	//	Build Queries Functions
	//-------------------------------------------	
	
	public function build_query()
	{
		$query = '';
		if (!empty($this->select))
		{
			$query .= 'SELECT ' . ($this->distinct ? 'DISTINCT ' : '');
			foreach ($this->select as $select)
			{
				$query .= "{$select},";
			}
			$query = rtrim($query, ',');
		}
		if (!empty($this->from))
		{
			$query .= " FROM {$this->from}";
		}
		if (!empty($this->joins))
		{
			foreach ($this->joins as $join)
			{
				$join_type = !empty($join['join_type']) ? "{$join['join_type']} ": '';
				$query .= " {$join_type}JOIN {$join['table']} ON {$join['on']}";
			}
		}
		if (!empty($this->where))
		{
			foreach ($this->where as $where)
			{
				$query .= $this->parse_where_statement($where['where'], $where['value']) . ' AND ';
			}
			$query = str_replace(' AND  WHERE', ' AND', $query);
			$query = rtrim($query, ' AND ');
		}
		if (!empty($this->order_by))
		{
			$query .= ' ORDER BY ';
			foreach ($this->order_by as $order_by)
			{
				$query .= "{$order_by['field']} {$order_by['direction']},";
			}
			$query = rtrim($query, ',');
		}
		if (!is_null($this->limit))
		{
			$query .= " LIMIT {$this->limit}";
		}
		if (!is_null($this->offset))
		{
			$query .= " OFFSET {$this->offset}";
		}
		$this->reset_query();
		return "{$query};";
	}

	public function get_compiled_insert($table, $reset = true)
	{
		if (empty($this->set))
		{
			throw new Exception('No values set to generate "insert"');
		}

		$query = "INSERT INTO {$table}";
		$fields = '';
		$values = '';
		foreach ($this->set as $set)
		{
			if (!is_null($set['value']))
			{
				$fields .= "{$set['key']},";
				$set['value'] = $this->parse_value($set['value']);
				$values .= "{$set['value']},";
			}
			else
			{
				if (is_assoc($set['key']))
				{
					foreach ($set['key'] as $key => $value)
					{
						$fields .= "{$key},";
						$value = $this->parse_value($value);
						$values .= "{$value},";
					}
				}
				else
					throw new Exception('Invalid argument for set in function "insert", should be a string or an associative array.');
			}
		}
		$fields = rtrim($fields, ',');		
		$values = rtrim($values, ',');		
		$query .= "({$fields}) VALUES({$values});";
	
		if ($reset)
		{
			$this->reset_query();
		}
		return $query;
	}

	public function get_compiled_update($table, $reset = true)
	{		
		if (empty($this->set))
		{
			throw new Exception('No values set to generate "insert"');
		}

		$query = "UPDATE {$table} SET ";
		$value_pair = '';
		foreach ($this->set as $set)
		{
			if (!is_null($set['value']))
			{
				$set['value'] = $this->parse_value($set['value']);
				$value_pair .= "{$set['key']} = {$set['value']},";
			}
			else
			{
				if (is_assoc($set['key']))
				{
					foreach ($set['key'] as $key => $value)
					{
						$value = $this->parse_value($value);
						$value_pair .= "{$key} = {$value},";
					}
				}
				else
					throw new Exception('Invalid argument for set in function "insert", should be a string or an associative array.');
			}
		}
		$query .= rtrim($value_pair, ',');
		foreach ($this->where as $where)
		{
			$query .= $this->parse_where_statement($where['where'], $where['value']);
		}
	
		if ($reset)
		{
			$this->reset_query();
		}
		return "{$query};";
	}

	public function get_compiled_replace($table, $reset = true)
	{
		$query = $this->get_compiled_update($table, $reset);
		$query = preg_replace('/UPDATE/', 'REPLACE INTO', $query, 1);
		if (strpos($query, ' WHERE'))
		{
			$query = substr($query, 0, strpos($query, ' WHERE'));
		}
		if (substr($query, -1) !== ';')
		{
			$query .= ';';
		}
		return $query;
	}

	public function get_compiled_delete($table, $reset = true)
	{
		$query = "DELETE FROM {$table}";
		foreach ($this->where as $where)
		{
			$query .= $this->parse_where_statement($where['where'], $where['value']);
		}
		if ($reset)
		{
			$this->reset_query();
		}		
		return "{$query};";
	}

	//-------------------------------------------
	//	Escaping Functions
	//-------------------------------------------

	public function escape_like($like)
	{
		return $this->db_driver->escape_string(str_replace(array('%', '_'), array('\\%', '\\_'), $like));
	}

	//-------------------------------------------
	//	Parsing Functions
	//-------------------------------------------

	private function parse_value($value)
	{
		if (is_string($value))
		{
			if (in_array($value, $this->db_functions, true))
				return $value;
			return "'".$this->db_driver->escape_string($value)."'";
		}
		else if (is_bool($value))
			return ((int)($value));
		else if ($value instanceof DateTime)
			return "'{$value->format('Y-m-d H:i:s')}'";
		else if (is_null($value))
			return 'NULL';
		else
			return $value;
	}

	private function parse_values($values)
	{
		$string_values = '';
		for ($i = 0, $length = count($values); $i < $length; $i++)
		{
			$string_values .= $this->parse_value($values[$i]).',';
		}
		return rtrim($string_values, ',');
	}

	private function parse_set(array $set)
	{
		if (is_assoc($set))
		{
			$set_string = '';
			foreach ($set as $key => $value)
			{
				$set_string .= "{$key}={$this->parse_value($value)}, ";
			}
			return rtrim($set_string, ', ');
		}
		else
			exit_system_error('Database Error: Function trying to parse a non associative array as argument.');
	}

	private function parse_where_statement($where, $value = null)
	{
		if (is_string($value))
			return " WHERE {$where} = {$this->parse_value($value)}";
		if (is_string($where))
			return " WHERE {$where}";
		if (is_assoc($where))
		{
			$where_statement = ' WHERE ';
			foreach ($where as $key => $value)
			{
				$where_statement .= "{$key} = {$this->parse_value($value)} AND ";
			}
			return rtrim($where_statement, 'AND ');
		}
	}
}

<?php

namespace Star\Database;

interface DbDriverInterface
{
	function escape_string($string);

	function set_timezone($timezone);
}
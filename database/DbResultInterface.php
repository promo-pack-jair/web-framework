<?php

namespace Star\Database;

interface DbResultInterface
{
	function fetch();

	function result();

	function num_rows();

	function row();

	function fetch_all($result_mode = null);
}
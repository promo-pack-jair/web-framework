<?php

namespace Star\Database;

abstract class DbDrivers
{
	const MYSQLI = 'mysqli';
    const PDO = 'pdo';
}
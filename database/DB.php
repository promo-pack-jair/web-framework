<?php

namespace Star\Database;
/**
 * DB
 *
 * Database access,
 * Notes: The following values are not automatically escaped therefore the user should properly escape the values
 *			-Insert/Replace/Update Values provided as string instead of arrays
 *			-Get variables: fields, limit, order_by, group_by, having
 *			-Like Clauses
 *			-Cache
 */

//Missing Insert Batch

class DB
{
	private $db_driver;
	private $query_builder;

	function __construct($db = null)
	{
		if ($db === null)
		{
			if (!file_exists($file_path = APP_PATH.'config/database.php'))
			{
				exit_system_error('The configuration file database.php does not exist.');
			}
			include($file_path);
		}
		try
		{
			switch ($db['driver'])
			{
				case DbDrivers::MYSQLI:
				{
					$this->db_driver = new \Star\Database\Drivers\MySQLi\MySQLiDriver($db['host'], $db['user'], $db['pass'], $db['database']);
		
					if ($this->db_driver->connect_error)
						exit_system_error("Failed to connect to MySQL: ({$this->db_driver->connect_errno}) {$this->db_driver->connect_error}");
		
					$this->db_driver->set_charset($db['char_set']);
					$this->query_builder = new QueryBuilder($this);
					break;
				}
				case DbDrivers::PDO:
				{
					break;
				}
				default:
				{
					exit_system_error("Driver \"{$db['driver']}\" not found for Database");
				}
			}
			if (!empty($db['timezone']))
			{
				$this->set_timezone($db['timezone']);
			}
		}
		catch (db_driver_sql_exception $e)
		{
			exit_system_error("DB Exception: {$e->errorMessage()}");
		}
	}

	function __destruct()
	{
		if ($this->db_driver)
		{
			$this->db_driver->close();
		}
	}

	public function get_db_driver()
	{
		return $this->db_driver;
	}

	public function get_query_builder()
	{
		return $this->query_builder;
	}

	public function insert_id()
	{
		return $this->db_driver->insert_id;
	}

	public function last_query()
	{
		return $this->db_driver->last_query;
	}

	//-------------------------------------------
	//	Action Methods
	//-------------------------------------------

	public function get($table, $fields = '*', $where = '', $limit = 0, $order_by = '', $group_by = '', $having = '', $distinct = false)
	{
		if ($distinct)
		{
			$this->distinct();
		}
		if (empty($this->query_builder->select))
		{
			$this->query_builder->select($fields);
		}
		$this->query_builder->from($table);
		if (!empty($where))
		{
			$this->where($where);
		}
		$this->query_builder->limit($limit);
		if (!empty($order_by))
		{
			$this->query_builder->order_by($order_by);
		}
		if (!empty($group_by))
		{
			$this->query_builder->group_by($group_by);
		}
		if (!empty($having))
		{
			//$this->query_builder->having($having);
		}
		return $this->query($this->query_builder->build_query());
	}

	public function get_where($table, $where, $fields = '*')
	{
		return $this->get($table, $fields, $where);
	}

	public function get_row($table, $where = null, $fields = '*')
	{
		$this->query_builder->from($table);
		if (!empty($where))
		{
			$this->query_builder->where($where);
		}
		if (empty($this->query_builder->select))
		{
			$this->query_builder->select($fields);
		}
		$this->query_builder->limit(1);

		if ($result = $this->db_driver->query($this->query_builder->build_query()))
		{
			if ($result->num_rows > 0)
				return $result->fetch_object();
		}
		return false;
	}

	public function get_distinct($table, $fields = '*', $where = '', $limit = '', $order_by = '', $group_by = '', $having = '')
	{
		return $this->get($table, $fields, $where, $limit, $order_by, $group_by, $having, true);
	}

	public function insert($table, $values)
	{
		$this->query_builder->set($values);
		$query = $this->query_builder->get_compiled_insert($table);
		return $this->execute_non_query($query);
	}

	public function update($table, $values, $where = null)
	{
		if ($where === null && empty($this->query_builder->where))
		{
			throw new \InvalidArgumentException('No values set for update function.');
		}
		if ($where !== null)
		{
			$this->query_builder->where($where);
		}
		$this->query_builder->set($values);
		$query = $this->query_builder->get_compiled_update($table);
		return $this->execute_non_query($query);
	}

	public function replace($table, $values)
	{
		$this->query_builder->set($values);
		$query = $this->query_builder->get_compiled_replace($table);
		return $this->execute_non_query($query);
	}

	public function insert_batch($table, $values)
	{
		/*if (is_assoc($values))
			$query = "INSERT INTO {$table}{$this->build_insert(array_keys($values), array_values($values))}";
		else if (is_array($values))
			$query = "INSERT INTO {$table} VALUES({$this->parse_values($values)});";

		return $this->execute_non_query($query);*/
	}

	public function delete($table, $where)
	{
		$this->query_builder->where($where);
		$query = $this->query_builder->get_compiled_delete($table);
		return $this->execute_non_query($query);
	}
	
	public function count_all_results($table)
	{
		$this->query_builder->from($table);
		$this->query_builder->select("COUNT(*) as count");
		$result = $this->query($this->query_builder->build_query());
		return $result->row()->count;
	}

	public function query($query)
	{
		$result = $this->db_driver->query($query);
		$this->query_builder->reset_query();
		return $result;
	}

	private function execute_non_query($query)
	{
		$result = $this->db_driver->query($query);
		$this->query_builder->reset_query();

		if ($result)
			return true;
		return false;
	}
	
	/**
	 * __call
	 *
	 * Allows seamless access to Query Builder methods
	 */
	function __call($method, $arguments)
	{
		return call_user_func_array(array($this->query_builder, $method), $arguments);
	}
}